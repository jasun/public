#!/bin/sh

### Used to build alpine image for GCP automagically
### https://docs.alpinelinux.org/user-handbook/0.1a/Installing/setup_alpine.html#_answer_files

export BOOTLOADER=grub
export ROOTFS=xfs
export DISKOPTS='-L -s 512 -m sys /dev/sda'

# Use US layout with US variant
export KEYMAPOPTS="us us"

export INTERFACESOPTS="auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp"

# Set hostname to alpine-test
export HOSTNAMEOPTS="-n gcp-image"

# set http/ftp proxy
export PROXYOPTS="none"

# Set timezone to UTC
export TIMEZONEOPTS="-z UTC"

# Add CDN repo
export APKREPOSOPTS="-1"

# Install Openssh
export SSHDOPTS="-c openssh"

# Use openntpd
export NTPOPTS="-c none"

setup-alpine -e
